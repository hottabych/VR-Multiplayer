﻿using Smooth;
using UnityEngine;
using UnityEngine.Networking;

public class BallKickPhysics : NetworkBehaviour
{
    public float impulseMultiplier = 7f;
    public float soundThreshold = 1f;
    [SyncVar]
    private Vector3 velocityToApply;
    [SyncVar]
    private Vector3 impulseContactPoint;
    new AudioSource audio;
    SmoothSync smoothSync;
    Rigidbody rb;


    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        smoothSync = GetComponent<SmoothSync>();
        rb = GetComponent<Rigidbody>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (!hasAuthority) return;

        //усиление удара по мячу
        if (collision.collider.CompareTag("FootCollider"))
        {
            velocityToApply = collision.rigidbody.velocity;
            impulseContactPoint = collision.contacts[0].point;
            //CmdKick();        //не будем делать пинок, потому что он не работает
        }

        if (collision.relativeVelocity.sqrMagnitude > soundThreshold * soundThreshold)
        {
            CmdHitGround();
        }
    }

    [Command]
    private void CmdKick()
    {
        RpcKick();
    }

    [ClientRpc]
    private void RpcKick()
    {        
        rb.AddForceAtPosition(velocityToApply * impulseMultiplier, impulseContactPoint, ForceMode.Impulse);
    }

    [Command]
    private void CmdHitGround()
    {
        RpcHitGround();
    }

    [ClientRpc]
    private void RpcHitGround()
    {        
        audio.Play();
    }


    //мяч в воротах
    private void OnTriggerEnter(Collider other)
    {
        if (!hasAuthority) return;

        int timestamp = NetworkTransport.GetNetworkTimestamp();   //для телепорта     

        if (other.CompareTag("GateRed"))
        {
            FreezeAndRespawn(timestamp);        //локально сразу телепортируем
            CmdAddScore(true, timestamp);
        }
        else if (other.CompareTag("GateBlue"))
        {
            FreezeAndRespawn(timestamp);
            CmdAddScore(false, timestamp);            
        }
    }


    [Command]
    private void CmdAddScore(bool red, int timestamp)
    {
        if (red)
        {
            ScoreManager.instance.AddPoints(true);
        }
        else
        {
            ScoreManager.instance.AddPoints(false);
        }

        RpcResetBall(timestamp);
    }

    [ClientRpc]
    private void RpcResetBall(int timestamp)
    {        
        ReferenceManager.instance.sfx.Play();       //свисток

        if (hasAuthority) return;       //на локальном уже произвели телепортацию
        FreezeAndRespawn(timestamp);
    }

    void FreezeAndRespawn(int timestamp)
    {        
        rb.isKinematic = true;
        smoothSync.teleport(timestamp, ScoreManager.instance.ballSpawnPoint.position, Quaternion.identity);
        rb.isKinematic = false;        
    }
}
