﻿using System.Collections;
using UnityEngine;

public class OnScreenLogger : MonoBehaviour
{
    public bool enableWarnings;
    public bool enableErrors;
    public float xPos = 300f, yPos = 0f;

    string myLog;
    Queue myLogQueue = new Queue();

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (!enableWarnings)
        {
            if (type == LogType.Warning) return;
        }

        if (!enableErrors)
        {
            if (type == LogType.Error ||
                type == LogType.Exception) return;
        }

        myLog = logString;
        string newString = "\n [" + type + "] : " + myLog;
        myLogQueue.Enqueue(newString);
        if (type == LogType.Exception)
        {
            newString = "\n" + stackTrace;
            myLogQueue.Enqueue(newString);
        }
        myLog = string.Empty;
        foreach (string mylog in myLogQueue)
        {
            myLog += mylog;
        }
    }

    void OnGUI()
    {           
        GUI.Label(new Rect(xPos, yPos, 400, Screen.height-yPos), myLog);
    }
}
