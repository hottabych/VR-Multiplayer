﻿using UnityEngine;
using UnityEngine.Networking;
using HTC.UnityPlugin.Vive;

public class Player : NetworkBehaviour
{
    public Transform HMD, rightHand, leftHand, hip, rightFoot, leftFoot;    
    public GameObject mesh;
    public bool debugColorMesh;


    private void Start()
    {
        if (!isLocalPlayer)
        {
            HMD.GetComponent<VivePoseTracker>().enabled = false;
            rightHand.GetComponent<VivePoseTracker>().enabled = false;
            leftHand.GetComponent<VivePoseTracker>().enabled = false;
            hip.GetComponent<VivePoseTracker>().enabled = false;
            rightFoot.GetComponent<VivePoseTracker>().enabled = false;
            leftFoot.GetComponent<VivePoseTracker>().enabled = false;
        }
    }

    public override void OnStartLocalPlayer()
    {
        // чтобы работали Network Start Positions
        var steamVrRig = VRTK.VRTK_SDKManager.instance.loadedSetup.actualBoundaries.transform;
        steamVrRig.SetPositionAndRotation(transform.position, transform.rotation);
        transform.SetParent(steamVrRig); //attach myself to SteamVR camera Rig    

        //раскрасить свой меш и девайсы -- для отладки
        if (debugColorMesh)
        {
            HMD.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            rightHand.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            leftHand.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            hip.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            rightFoot.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            leftFoot.GetComponentInChildren<MeshRenderer>().material.color = Color.blue;
            mesh.GetComponent<SkinnedMeshRenderer>().material.color = Color.green;
        }
    }

    public void HideDeviceMeshes()
    {
        rightHand.GetChild(0).gameObject.SetActive(false);
        leftHand.GetChild(0).gameObject.SetActive(false);
        hip.GetChild(0).gameObject.SetActive(false);
        rightFoot.GetChild(0).gameObject.SetActive(false);
        leftFoot.GetChild(0).gameObject.SetActive(false);
    }
}
