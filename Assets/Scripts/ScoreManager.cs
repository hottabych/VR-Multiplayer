﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ScoreManager : NetworkBehaviour
{
    public static ScoreManager instance;

    [SyncVar(hook = "OnChangeScoreRed")]    
    public int scorePlayerRed = 0;
    [SyncVar(hook = "OnChangeScoreBlue")]    
    public int scorePlayerBlue = 0;
    public Text scoreText;
    public Transform ballSpawnPoint;    

    private void Awake()
    {
        instance = this;
    }

    
    //вызывается из Cmd BallKickPhysics
    public void AddPoints(bool red)
    {
        if (red)
        {
            scorePlayerRed++;            
        }
        else    // if blue
        {
            scorePlayerBlue++;
        }        
    }

    void OnChangeScoreRed(int score)
    {
        scorePlayerRed = score;     //manually putting the change into member variable in hook function
        scoreText.text = scorePlayerRed + ":" + scorePlayerBlue;
    }

    void OnChangeScoreBlue(int score)
    {
        scorePlayerBlue = score;
        scoreText.text = scorePlayerRed + ":" + scorePlayerBlue;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();        
        OnChangeScoreRed(scorePlayerRed);
        OnChangeScoreBlue(scorePlayerBlue);
    }
}
