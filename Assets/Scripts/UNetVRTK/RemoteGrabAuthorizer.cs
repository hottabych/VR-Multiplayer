﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using VRTK;
using UNetVRTK;

/// <summary>
/// Simply assigns/removes authority on Grab
/// </summary>
public class RemoteGrabAuthorizer : NetworkBehaviour
{
    private VRTK_InteractableObject interactableObject;
    private NetworkIdentity networkIdentity;
    private Rigidbody rb;

    //temp variables
    private Vector3 lastPosition;
    private Quaternion lastRotation;

    [HideInInspector]
    [SyncVar(hook = "OnIsGrabbedUpdate")]
    public bool isGrabbed;

    void Awake()
    {
        interactableObject = GetComponent<VRTK_InteractableObject>();
        networkIdentity = GetComponent<NetworkIdentity>();
        rb = GetComponent<Rigidbody>();
    }

    public void OnEnable()
    {
        interactableObject.InteractableObjectGrabbed += HandleGrab;
        interactableObject.InteractableObjectUngrabbed += HandleUngrab;
    }

    public void OnDisable()
    {
        interactableObject.InteractableObjectGrabbed -= HandleGrab;
        interactableObject.InteractableObjectUngrabbed -= HandleUngrab;
    }


    private void HandleGrab(object sender, InteractableObjectEventArgs e)
    {
        lastPosition = transform.position;
        lastRotation = transform.rotation;
        
        VRTKNetworkBridge.localPlayerVRTKBridge.CmdReassignAuthority(networkIdentity);
        StartCoroutine(WaitForAuthority(sender, DateTime.Now.AddMilliseconds(500)));
    }

    private IEnumerator WaitForAuthority(object sender, DateTime until)
    {
        yield return new WaitUntil(() => hasAuthority || DateTime.Now > until);
        if (hasAuthority)
        {
            CmdSetGrab(true);
        }
        else
        {
            ((VRTK_InteractableObject)sender).ForceStopInteracting();
            transform.position = lastPosition;
            transform.rotation = lastRotation;
        }
    }

    [Command]
    private void CmdSetGrab(bool isGrabbed)
    {
        this.isGrabbed = isGrabbed;     //реально переменная класса не изменится, а значение передастся в хук
    }

    private void HandleUngrab(object sender, InteractableObjectEventArgs e)
    {
        if (hasAuthority)
        {
            CmdSetGrab(false);
        }
    }

    //хук
    public void OnIsGrabbedUpdate(bool isGrabbed)
    {        
        rb.useGravity = !isGrabbed;
    }
}
