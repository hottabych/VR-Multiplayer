﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using VRTK.Controllables;

public class ButtonSphereSpawner : NetworkBehaviour
{
    public VRTK_BaseControllable controllable;
    public Text displayText;

    public GameObject spherePrefab;
    public Transform spawnPoint;


    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);
        controllable.ValueChanged += ValueChanged;
        controllable.MaxLimitReached += MaxLimitReached;
        controllable.MinLimitReached += MinLimitReached;
    }

    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        if (displayText != null)
        {
            displayText.text = e.value.ToString("F1");
        }
    }

    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        CmdSpawnSphere();
    }

    protected virtual void MinLimitReached(object sender, ControllableEventArgs e)
    {

    }


    [Command]
    void CmdSpawnSphere()
    {
        if (spawnPoint)
        {
            var newSphere = Instantiate(spherePrefab, spawnPoint.position, Quaternion.identity);
            NetworkServer.Spawn(newSphere);
            Destroy(newSphere, 5f);
        }
    }
}
