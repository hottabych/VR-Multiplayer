﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class EnemyHealth : NetworkBehaviour
{
    public int maxHealth = 100;
    [SyncVar(hook = "OnChangeHealth")]
    public int currentHealth;
    public RectTransform healthBar;
    public bool destroyOnDeath = true;
    Animator anim;

    private void Awake()
    {
        currentHealth = maxHealth;
        anim = GetComponent<Animator>();
    }

    public void TakeDamage(int amount)
    {
        if (!isServer)
            return;

        currentHealth -= amount;
        if (currentHealth <= 0)
        {            
            Debug.Log("Enemy dead!");
            anim.SetTrigger("Die");
            if (destroyOnDeath)
            {
                Destroy(gameObject, 3f);
            }
        }
    }


    void OnChangeHealth(int health)
    {
        currentHealth = health;
        healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
    }


    public override void OnStartClient()
    {
        base.OnStartClient();
        OnChangeHealth(currentHealth);
    }
}