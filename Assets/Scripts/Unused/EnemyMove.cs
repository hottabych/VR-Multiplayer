﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

public class EnemyMove : MonoBehaviour
{
    public Transform nodesGroup;
    private List<Transform> nodes;
    public float closeDistance = .5f;    
    public float destroyTime = 4f;
    NavMeshAgent agent;
    Animator anim;
    private int destPoint = 0;
    Vector3 target;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        anim = GetComponent<Animator>();
        foreach (Transform node in nodesGroup)
        {
            nodes.Add(node);
        }
    }


    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (nodes.Count == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = nodes[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % nodes.Count;
    }


    void Update()
    {       
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (!agent.pathPending && agent.remainingDistance < closeDistance)
            GotoNextPoint();
    }


    private void Kill()
    {        
        anim.SetTrigger("Kill");
        agent.isStopped = true;
        Destroy(gameObject, destroyTime);
    }

}
