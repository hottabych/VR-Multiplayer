﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class GunBeam : MonoBehaviour
{
    public VRTK_InteractableObject linkedObject;
    public Transform laserOrigin;

    protected virtual void OnEnable()
    {
        linkedObject = (linkedObject == null ? GetComponent<VRTK_InteractableObject>() : linkedObject);        

        if (linkedObject != null)
        {
            linkedObject.InteractableObjectGrabbed += EnableBeam;
            linkedObject.InteractableObjectUngrabbed += DisableBeam;
        }
    }

    protected virtual void OnDisable()
    {
        if (linkedObject != null)
        {
            linkedObject.InteractableObjectGrabbed -= EnableBeam;
            linkedObject.InteractableObjectUngrabbed -= DisableBeam;
        }
    }

    public void EnableBeam(object sender, InteractableObjectEventArgs e)
    {
        GlobalControllers.instance.laserBeam.enabled = true;
        GlobalControllers.instance.laserBeam.customOrigin = laserOrigin;
    }

    public void DisableBeam(object sender, InteractableObjectEventArgs e)
    {
        GlobalControllers.instance.laserBeam.enabled = false;        
    }
}
