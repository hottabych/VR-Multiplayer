﻿using UnityEngine;
using VRTK;

public class InteractableEnemy : MonoBehaviour
{
    public VRTK_InteractableObject linkedObject;
    public EnemyHealth health;

    protected virtual void OnEnable()
    {        
        linkedObject = (linkedObject == null ? GetComponent<VRTK_InteractableObject>() : linkedObject);
        if (!health) health = GetComponent<EnemyHealth>();

        if (linkedObject != null)
        {
            linkedObject.InteractableObjectUsed += InteractableObjectUsed;
            linkedObject.InteractableObjectUnused += InteractableObjectUnused;
        }        
    }

    protected virtual void OnDisable()
    {
        if (linkedObject != null)
        {
            linkedObject.InteractableObjectUsed -= InteractableObjectUsed;
            linkedObject.InteractableObjectUnused -= InteractableObjectUnused;
        }
    }

    // 1ый клик -- used; 2й -- unused
    protected virtual void InteractableObjectUsed(object sender, InteractableObjectEventArgs e)
    {
        if (health)
        {
            health.TakeDamage(10);
        }
    }

    protected virtual void InteractableObjectUnused(object sender, InteractableObjectEventArgs e)
    {
        InteractableObjectUsed(sender, e);
    }
}
