﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using VRTK.Controllables;
using UNetVRTK;

public class LeverLamp : NetworkBehaviour
{
    public VRTK_BaseControllable controllable;
    public Text displayText;

    public GameObject lamp;
    public Color offColor, onColor;

    [SyncVar (hook = "OnLampOnUpdate")]
    public bool lampOn;

    [SyncVar(hook = "OnChangeValue")]
    public float leverValue;


    protected virtual void OnEnable()
    {
        controllable = (controllable == null ? GetComponent<VRTK_BaseControllable>() : controllable);
        controllable.ValueChanged += ValueChanged;
        controllable.MaxLimitReached += MaxLimitReached;
        controllable.MinLimitReached += MinLimitReached;
    }

    //controllable event handler
    protected virtual void ValueChanged(object sender, ControllableEventArgs e)
    {
        CmdLeverValueChange(e.value);
    }

    [Command]
    void CmdLeverValueChange(float value)
    {
        leverValue = value;
    }

    //syncvar hook
    private void OnChangeValue(float value)
    {
        if (displayText != null)
        {
            displayText.text = value.ToString("F1");
        }
    }

    protected virtual void MaxLimitReached(object sender, ControllableEventArgs e)
    {
        CmdSwitchLight(true);
    }

    protected virtual void MinLimitReached(object sender, ControllableEventArgs e)
    {
        CmdSwitchLight(false);
    }

    [Command]
    void CmdSwitchLight(bool on)
    {
        lampOn = on;
    }

    //syncvar hook
    void OnLampOnUpdate(bool on)
    {
        if (on)
        {
            lamp.GetComponent<MeshRenderer>().material.color = onColor;
        }
        else
        {
            lamp.GetComponent<MeshRenderer>().material.color = offColor;
        }
    }


    public void HandleGrab()
    {
        VRTKNetworkBridge.localPlayerVRTKBridge.CmdAssignAuthority(GetComponent<NetworkIdentity>());
    }

    public void HandleUngrab()
    {
        if (hasAuthority)
        {
            VRTKNetworkBridge.localPlayerVRTKBridge.CmdRemoveAuthority(GetComponent<NetworkIdentity>());
        }
    }
}
