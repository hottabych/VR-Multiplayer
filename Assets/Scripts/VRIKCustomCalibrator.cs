﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;
using UnityEngine.Networking;
using HTC.UnityPlugin.Vive;


public class VRIKCustomCalibrator : NetworkBehaviour
{

    [Tooltip("Reference to the VRIK component on the avatar.")] public VRIK ik;
	[Tooltip("The settings for VRIK calibration.")] public VRIKCalibrator.Settings settings;
    [Tooltip("The HMD.")] public Transform headTracker;
	[Tooltip("(Optional) A tracker placed anywhere on the body of the player, preferrably close to the pelvis, on the belt area.")] public Transform bodyTracker;
	[Tooltip("(Optional) A tracker or hand controller device placed anywhere on or in the player's left hand.")] public Transform leftHandTracker;
	[Tooltip("(Optional) A tracker or hand controller device placed anywhere on or in the player's right hand.")] public Transform rightHandTracker;
	[Tooltip("(Optional) A tracker placed anywhere on the ankle or toes of the player's left leg.")] public Transform leftFootTracker;
	[Tooltip("(Optional) A tracker placed anywhere on the ankle or toes of the player's right leg.")] public Transform rightFootTracker;

    bool ikDone = false;
    float ikDoneTimer;
    float ikDoneTimerDef = 1f;

    public void Calibrate()
    {
        ikDone = true;
        VRIKCalibrator.Calibrate(ik, settings, headTracker, bodyTracker, leftHandTracker, rightHandTracker, leftFootTracker, rightFootTracker);
        GetComponent<Player>().HideDeviceMeshes();
    }

    void Update()
    {
        if (!isLocalPlayer) return;

        if (ikDone) return;

        if (ViveInput.GetPress(HandRole.RightHand, ControllerButton.Trigger) &&
            ViveInput.GetPress(HandRole.LeftHand, ControllerButton.Trigger))
        {
            CmdCalibrate();            
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            CmdCalibrate();
        }
    }

    [Command]
    void CmdCalibrate()
    {
        RpcCalibrate();
    }

    [ClientRpc]
    void RpcCalibrate()
    {
        Calibrate();
    }
}

